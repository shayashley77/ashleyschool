const cardItems = document.querySelectorAll('.card-front');
let result=0;
let numberAnswered=0

cardItems.forEach((cardItem, i)=>{
    cardItem.addEventListener('click', function () {
        cardItem.parentElement.classList.toggle('is-flipped');
    });
})

answers = ["T","T","F","T","T","F","F","F","T"];

function submitAnswer(userResponse, el){
    let card=el;
    let cardParent = card.parentElement;
    let cardParentId = card.parentElement.id;
    let idArray = cardParentId.split('-');
    let arrayPosition = idArray[2] - 1;
    let correctAnswer = answers[arrayPosition];
    cardParent.parentElement.style.display='none';
    numberAnswered++
    if (correctAnswer == userResponse) {
        result++;
    }
    else {
        cardParent.closest(".card").style.backgroundColor="#060606";
        cardParent.closest(".card").style.opacity=".99";
    }
    if (numberAnswered == 9) {
        document.getElementById("triviaResults").innerHTML="You got " + result + " out of 9 Goldblums! Try Again!";
    }
}

function changeWinningJeff (option) {
    console.log("hello");
    let optionValue=option.value;
    console.log(optionValue, "option-value");
    let imageArray=["jeff-jurassic-park-3.png", "jeff-independence2.webp", "jeff-world-3.jpg", "jeff-thor2.webp", "jeff-the-fly-6.jpg"];
    const matches = imageArray.filter(s => s.includes(optionValue));
    let imagePath=matches[0];
    console.log(imagePath, "image-path");
    document.getElementById("movie-option").style.
    backgroundImage="url("+imagePath+")";
    console.log(document.getElementById("movie-option"))
}

